'use strict'
const addButton = document.getElementById('addButton');
const inputField = document.getElementById('input');
const toDoList = document.getElementById('toDoList');
const delItem = document.getElementById('deleteItem');
const itemValue = document.getElementById('listImput');

let items;
!localStorage.items ? items = [] : items = JSON.parse(localStorage.getItem('items'));

let elements;
let description;

function CreateItem(description) {
	this.description = description;
	this.completed = false;
}

const newTemplate = (item, index) => {
	return `
		<div class="listItem ${item.completed ? 'completed' : ''}" id="listItem">
			<span class="listInput" id="listInput" oncontextmenu="renameItem();return false"
			onmousedown="return false" ondblclick="completedItem(${index})">${item.description}</span>
			<button onclick="deleteItem(${index})" class="deleteItem" id="deleteItem">X</button>
		</div>	
	`
}
const renameItem = () => {
};

const itemList = () => {
	const activeItem = items.length && items.filter(item => item.completed == false);
	const completedItem = items.length && items.filter(item => item.completed == true);
	items = [...activeItem, ...completedItem];
}

const addHtml = () => {
	toDoList.innerHTML = "";
	if (items.length > 0) {
		itemList();
		items.forEach((element, index) => {
			toDoList.innerHTML += newTemplate(element, index);
		})
		elements = document.querySelectorAll('.listItem');
	}
}

addHtml();

const saveLocal = () => {
	localStorage.setItem('items', JSON.stringify(items));
}

const completedItem = index => {
	items[index].completed = !items[index].completed;
	if (items[index].completed) {
		elements[index].classList.add('completed');
	}
	else {
		elements[index].classList.remove('completed');
	}
	saveLocal();
	addHtml();
}

addButton.addEventListener('click', () => {
	items.push(new CreateItem(inputField.value));
	saveLocal();
	addHtml();
	inputField.value = "";
})

const enterDown = (event) => {
	if (event.keyCode == 13) {
		items.push(new CreateItem(inputField.value));
		saveLocal();
		addHtml();
		inputField.value = "";
	}
}

const deleteItem = index => {
	items.splice(index, 1);
	saveLocal();
	addHtml();
}